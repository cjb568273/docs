# Auto-deploy - How to force package re-tagging

If the `auto_deploy:start` job fails, it may not always be possible to
restart it. In situations like this it will be better to tag again
using `/chatops run auto_deploy tag`.

If the first `auto_deploy:start` run managed to commit the product
version to
[`release/metadata`](https://ops.gitlab.net/gitlab-org/release/metadata)
it will be necessary to first revert the last commit or there will be
nothing new to tag.
